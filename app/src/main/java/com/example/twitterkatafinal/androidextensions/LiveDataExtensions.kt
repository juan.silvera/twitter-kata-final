package com.example.twitterkatafinal.androidextensions

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer


fun <T> LiveData<T>.onNewValueForOwner(owner: LifecycleOwner, action: (T) -> Unit) {
    this.observe(owner, Observer { it?.apply { action(it) } })
}

fun <T> LifecycleOwner.onChange(liveData: LiveData<T>, onChange: (T) -> Unit) {
    liveData.observe(this, Observer { it?.also { onChange(it) } })
}

fun <T> AppCompatActivity.onChange(liveData: LiveData<T>, onChange: (T) -> Unit) {
    liveData.observe(this, Observer { it?.also { onChange(it) } })
}

fun <T> Fragment.onChange(liveData: LiveData<T>, onChange: (T) -> Unit) {
    liveData.observe(viewLifecycleOwner, Observer { it?.also { onChange(it) } })
}