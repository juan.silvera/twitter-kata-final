package com.example.twitterkatafinal.infraestructure.models

import kotlinx.serialization.Serializable

@Serializable
data class UserTweetResponse(
    val nickname: String,
    val userTweet: String
)