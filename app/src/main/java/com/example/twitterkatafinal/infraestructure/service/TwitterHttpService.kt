package com.example.twitterkatafinal.infraestructure.service

import com.example.twitterkatafinal.core.domain.FollowUserRequest
import com.example.twitterkatafinal.core.domain.RegisterRequest
import com.example.twitterkatafinal.core.domain.UpdateUserRequest
import com.example.twitterkatafinal.core.domain.UserTweetRequest
import com.example.twitterkatafinal.core.service.UserService
import com.example.twitterkatafinal.infraestructure.client.TwitterAPIClient
import com.example.twitterkatafinal.infraestructure.models.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class TwitterHttpService(private val twitterAPIClient: TwitterAPIClient) : UserService {
    override suspend fun register(user: RegisterRequest): RegisterUserResponse? {
        val response = twitterAPIClient.register(user)

        val responseCode = response.code()

        return if (responseCode == 200) {
            return Json.decodeFromString<RegisterUserResponse>(response.body()!!)
        } else {
            null
        }
    }

    override suspend fun update(user: UpdateUserRequest): UpdateUserResponse? {
        val response = twitterAPIClient.update(user)

        val responseCode = response.code()

        return if (responseCode == 200) {
            return Json.decodeFromString<UpdateUserResponse>(response.body()!!)
        } else {
            null
        }
    }

    override suspend fun follow(user: FollowUserRequest): FollowUserResponse? {
        val response = twitterAPIClient.follow(user)

        val responseCode = response.code()

        return if (responseCode == 200) {
            return Json.decodeFromString<FollowUserResponse>(response.body()!!)
        } else {
            null
        }
    }

    override suspend fun tweet(user: UserTweetRequest): UserTweetResponse? {
        val response = twitterAPIClient.tweet(user)

        val responseCode = response.code()

        return if (responseCode == 200) {
            return Json.decodeFromString<UserTweetResponse>(response.body()!!)
        } else {
            null
        }
    }

    override suspend fun retrieveFollowedUsers(nickname: String): RetrieveFollowedUsersResponse? {
        val response = twitterAPIClient.followed(nickname)

        val responseCode = response.code()

        return if (responseCode == 200) {
            return Json.decodeFromString<RetrieveFollowedUsersResponse>(response.body()!!)
        } else {
            null
        }
    }

    override suspend fun retrieveTweets(nickname: String): UserTweetsResponse? {
        val response = twitterAPIClient.tweets(nickname)

        val responseCode = response.code()

        return if (responseCode == 200) {
            return Json.decodeFromString<UserTweetsResponse>(response.body()!!)
        } else {
            null
        }
    }
}