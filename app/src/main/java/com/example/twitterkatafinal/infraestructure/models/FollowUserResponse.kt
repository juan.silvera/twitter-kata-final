package com.example.twitterkatafinal.infraestructure.models

import kotlinx.serialization.Serializable

@Serializable
data class FollowUserResponse(
    val nickname: String,
    val userToFollow: String
)