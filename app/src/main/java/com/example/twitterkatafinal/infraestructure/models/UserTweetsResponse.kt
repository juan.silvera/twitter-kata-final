package com.example.twitterkatafinal.infraestructure.models

import kotlinx.serialization.Serializable

@Serializable
data class UserTweetsResponse(
    val userTweets: MutableList<String>
)
