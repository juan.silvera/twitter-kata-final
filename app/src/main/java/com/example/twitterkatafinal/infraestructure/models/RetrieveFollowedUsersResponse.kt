package com.example.twitterkatafinal.infraestructure.models

import kotlinx.serialization.Serializable

@Serializable
data class RetrieveFollowedUsersResponse(
    val followedUsers: MutableList<String>
)
