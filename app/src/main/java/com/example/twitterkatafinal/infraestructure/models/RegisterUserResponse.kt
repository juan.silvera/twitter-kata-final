package com.example.twitterkatafinal.infraestructure.models

import kotlinx.serialization.Serializable

@Serializable
data class RegisterUserResponse(
    var name: String,
    val nickname: String
)
