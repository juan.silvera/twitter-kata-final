package com.example.twitterkatafinal.infraestructure.client

import com.example.twitterkatafinal.core.domain.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface TwitterAPIClient {
    @POST("users/register")
    suspend fun register(@Body user: RegisterRequest): Response<String>

    @POST("users/update")
    suspend fun update(@Body user: UpdateUserRequest): Response<String>

    @POST("users/follow")
    suspend fun follow(@Body user: FollowUserRequest): Response<String>

    @GET("users/followed")
    suspend fun followed(@Query("nickname") nickname: String): Response<String>

    @POST("users/tweet")
    suspend fun tweet(@Body user: UserTweetRequest): Response<String>

    @GET("users/tweets")
    suspend fun tweets(@Query("nickname") nickname: String): Response<String>
}