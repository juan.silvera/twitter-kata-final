package com.example.twitterkatafinal.core.representation

data class RetrieveFollowedUsersRepresentation(
    val followedUsers: MutableList<String>
)