package com.example.twitterkatafinal.core.domain

data class RegisterRequest(
    var name: String,
    val nickname: String
)
