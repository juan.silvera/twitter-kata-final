package com.example.twitterkatafinal.core.representation

data class FollowUserRepresentation(
    val userToFollow: String
)
