package com.example.twitterkatafinal.core.exceptions

data class TwitterKataException(val code: String, override val message: String) : Throwable()