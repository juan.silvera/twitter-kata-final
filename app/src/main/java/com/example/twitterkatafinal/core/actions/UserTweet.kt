package com.example.twitterkatafinal.core.actions

import com.example.twitterkatafinal.core.representation.UserTweetRepresentation
import com.example.twitterkatafinal.core.domain.UserTweetRequest
import com.example.twitterkatafinal.core.exceptions.TwitterKataException
import com.example.twitterkatafinal.core.service.UserService

class UserTweet(private val userService: UserService) {
    suspend operator fun invoke(nickname: String, tweet: String): UserTweetRepresentation {
        userService.tweet(UserTweetRequest(nickname, tweet))
            ?: throw TwitterKataException(
                "777",
                "Your nickname does not exist"
            )

        return UserTweetRepresentation(tweet)
    }
}
