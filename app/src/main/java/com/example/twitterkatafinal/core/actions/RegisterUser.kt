package com.example.twitterkatafinal.core.actions

import com.example.twitterkatafinal.core.domain.RegisterRequest
import com.example.twitterkatafinal.core.representation.RegisterUserRepresentation
import com.example.twitterkatafinal.core.exceptions.TwitterKataException
import com.example.twitterkatafinal.core.service.UserService

class RegisterUser(private val userService: UserService) {

    suspend operator fun invoke(name: String, nickname: String): RegisterUserRepresentation {
        userService.register(RegisterRequest(name, nickname)) ?:
            throw TwitterKataException("666", "User already exists")

        return RegisterUserRepresentation(nickname)
    }
}