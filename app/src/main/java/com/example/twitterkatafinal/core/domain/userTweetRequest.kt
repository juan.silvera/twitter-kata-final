package com.example.twitterkatafinal.core.domain

data class UserTweetRequest(
    val nickname: String,
    val userTweet: String
)
