package com.example.twitterkatafinal.core.domain

data class FollowUserRequest(
    val nickname: String,
    val userToFollow: String
)
