package com.example.twitterkatafinal.core.service

import com.example.twitterkatafinal.core.domain.*
import com.example.twitterkatafinal.infraestructure.models.*

interface UserService {
    suspend fun register(user: RegisterRequest): RegisterUserResponse?
    suspend fun update(user: UpdateUserRequest): UpdateUserResponse?
    suspend fun follow(user: FollowUserRequest): FollowUserResponse?
    suspend fun retrieveFollowedUsers(nickname: String): RetrieveFollowedUsersResponse?
    suspend fun tweet(user: UserTweetRequest): UserTweetResponse?
    suspend fun retrieveTweets(nickname: String): UserTweetsResponse?
}