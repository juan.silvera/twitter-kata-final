package com.example.twitterkatafinal.core.providers

import com.example.twitterkatafinal.core.actions.*
import com.example.twitterkatafinal.presentation.factory.TwitterActionsFactory

open class ActionsProvider {
    open fun createRegisterUser(): RegisterUser {
        return TwitterActionsFactory.createRegisterUser()
    }

    open fun createUpdateUser(): UpdateUser {
        return TwitterActionsFactory.createUpdateUser()
    }

    open fun createFollowUser(): FollowUser {
        return TwitterActionsFactory.createFollowUser()
    }

    open fun createRetrieveFollowedUsers(): RetrieveFollowedUsers {
        return TwitterActionsFactory.createGetFollowedUsers()
    }

    open fun createUserTweet(): UserTweet {
        return TwitterActionsFactory.createUserTweet()
    }

    open fun createRetrieveUserTweets(): RetrieveUserTweets {
        return TwitterActionsFactory.createGetUserTweets()
    }
}