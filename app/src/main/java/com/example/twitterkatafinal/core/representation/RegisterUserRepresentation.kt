package com.example.twitterkatafinal.core.representation

data class RegisterUserRepresentation(
    val nickname: String
)
