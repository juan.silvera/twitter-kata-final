package com.example.twitterkatafinal.core.representation

data class RetrieveUserTweetsRepresentation(
    val userTweets: MutableList<String>
)