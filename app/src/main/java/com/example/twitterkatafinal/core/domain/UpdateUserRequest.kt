package com.example.twitterkatafinal.core.domain

data class UpdateUserRequest(
    var name: String,
    val nickname: String
)
