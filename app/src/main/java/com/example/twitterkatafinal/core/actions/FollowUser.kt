package com.example.twitterkatafinal.core.actions

import com.example.twitterkatafinal.core.representation.FollowUserRepresentation
import com.example.twitterkatafinal.core.domain.FollowUserRequest
import com.example.twitterkatafinal.core.exceptions.TwitterKataException
import com.example.twitterkatafinal.core.service.UserService

class FollowUser(private val userService: UserService) {
    suspend operator fun invoke(nickname: String, userToFollow: String): FollowUserRepresentation {
        userService.follow(FollowUserRequest(nickname, userToFollow))
            ?: throw TwitterKataException(
                "777",
                "Your nickname does not exist"
            )

        return FollowUserRepresentation(userToFollow)
    }
}
