package com.example.twitterkatafinal.core.representation

data class UserTweetRepresentation(
    var tweet: String
)
