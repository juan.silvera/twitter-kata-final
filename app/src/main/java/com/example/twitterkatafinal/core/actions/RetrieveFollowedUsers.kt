package com.example.twitterkatafinal.core.actions

import com.example.twitterkatafinal.core.exceptions.TwitterKataException
import com.example.twitterkatafinal.core.representation.RetrieveFollowedUsersRepresentation
import com.example.twitterkatafinal.core.service.UserService

class RetrieveFollowedUsers(private val userService: UserService) {
    suspend operator fun invoke(nickname: String): RetrieveFollowedUsersRepresentation {
        val userFound = userService.retrieveFollowedUsers(nickname) ?: throw TwitterKataException(
            "777",
            "Your nickname does not exist"
        )

        return RetrieveFollowedUsersRepresentation(userFound.followedUsers)
    }
}
