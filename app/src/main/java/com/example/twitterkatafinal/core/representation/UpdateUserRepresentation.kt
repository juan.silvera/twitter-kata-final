package com.example.twitterkatafinal.core.representation

data class UpdateUserRepresentation(
    var name: String
)
