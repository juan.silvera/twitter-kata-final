package com.example.twitterkatafinal.core.actions

import com.example.twitterkatafinal.core.exceptions.TwitterKataException
import com.example.twitterkatafinal.core.representation.RetrieveUserTweetsRepresentation
import com.example.twitterkatafinal.core.service.UserService

class RetrieveUserTweets(private val userService: UserService) {
    suspend operator fun invoke(nickname: String): RetrieveUserTweetsRepresentation {
        val userFound = userService.retrieveTweets(nickname) ?: throw TwitterKataException(
            "1111",
            "Your nickname does not exist"
        )

        return RetrieveUserTweetsRepresentation(userFound.userTweets)
    }
}
