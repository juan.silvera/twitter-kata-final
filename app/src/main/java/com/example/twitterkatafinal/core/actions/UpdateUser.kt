package com.example.twitterkatafinal.core.actions

import com.example.twitterkatafinal.core.representation.UpdateUserRepresentation
import com.example.twitterkatafinal.core.domain.UpdateUserRequest
import com.example.twitterkatafinal.core.exceptions.TwitterKataException
import com.example.twitterkatafinal.core.service.UserService

class UpdateUser(private val userService: UserService) {
    suspend operator fun invoke(name: String, nickname: String): UpdateUserRepresentation {
        userService.update(UpdateUserRequest(name, nickname))
            ?: throw TwitterKataException("777", "Your nickname does not exist")

        return UpdateUserRepresentation(name)
    }
}
