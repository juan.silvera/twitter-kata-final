package com.example.twitterkatafinal.presentation.factory

import com.example.twitterkatafinal.core.actions.*
import com.example.twitterkatafinal.core.service.UserService
import com.example.twitterkatafinal.infraestructure.client.TwitterAPIClient
import com.example.twitterkatafinal.infraestructure.service.TwitterHttpService
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

internal object TwitterActionsFactory {
    private val twitterAPIClient = getRetrofitClient().create(TwitterAPIClient::class.java)
    private val userService: UserService = TwitterHttpService(twitterAPIClient)

    fun createRegisterUser(): RegisterUser {
        return RegisterUser(
            userService = userService
        )
    }

    fun createUpdateUser(): UpdateUser {
        return UpdateUser(
            userService = userService
        )
    }

    fun createFollowUser(): FollowUser {
        return FollowUser(
            userService = userService
        )
    }

    fun createGetFollowedUsers(): RetrieveFollowedUsers {
        return RetrieveFollowedUsers(
            userService = userService
        )
    }

    fun createUserTweet(): UserTweet {
        return UserTweet(
            userService = userService
        )
    }

    fun createGetUserTweets(): RetrieveUserTweets {
        return RetrieveUserTweets(
            userService = userService
        )
    }

    private fun getRetrofitClient(): Retrofit {
        val gsonBuilder = GsonBuilder().setLenient().create()
        return Retrofit.Builder()
            .baseUrl("http://192.168.100.9:4567/api/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gsonBuilder))
            .build()
    }
}