package com.example.twitterkatafinal.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.twitterkatafinal.androidextensions.runSuspendCatching
import com.example.twitterkatafinal.core.actions.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TwitterViewModel(
    private val registerUserAction: RegisterUser,
    private val updateUserAction: UpdateUser,
    private val followUserAction: FollowUser,
    private val retrieveFollowedUsersAction: RetrieveFollowedUsers,
    private val userTweetAction: UserTweet,
    private val retrieveUserTweetsAction: RetrieveUserTweets,
    private val dispatcher: CoroutineDispatcher = Dispatchers.Main
) : ViewModel() {

    private val _message = MutableLiveData<String>()
    val message: LiveData<String> = _message

    private val _getMessages = MutableLiveData<List<String>>()
    val getMessages: LiveData<List<String>> = _getMessages

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    fun onRegisterButtonClick(name: String, nickname: String) {
        viewModelScope.launch {
            withContext(dispatcher) {
                registerUser(name, nickname)
            }
        }
    }

    fun onUpdateButtonClick(name: String, nickname: String) {
        viewModelScope.launch {
            withContext(dispatcher) {
                updateUser(name, nickname)
            }
        }
    }

    fun onFollowButtonClick(nickname: String, userToFollow: String) {
        viewModelScope.launch {
            withContext(dispatcher) {
                followUser(nickname, userToFollow)
            }
        }
    }

    fun onGetFollowedUsersButtonClick(nickname: String) {
        viewModelScope.launch {
            withContext(dispatcher) {
                getFollowedUsers(nickname)
            }
        }
    }

    fun onTweetButtonClick(nickname: String, tweetMessage: String) {
        viewModelScope.launch {
            withContext(dispatcher) {
                tweet(nickname, tweetMessage)
            }
        }
    }

    fun onGetUserTweetsButtonClick(nickname: String) {
        viewModelScope.launch {
            withContext(dispatcher) {
                getUserTweets(nickname)
            }
        }
    }

    private suspend fun registerUser(name: String, nickname: String) {
        runSuspendCatching {
            val result = registerUserAction(name, nickname)
            this._message.postValue("Username '${result.nickname}' registered successfully")
        }.onFailure {
            showError(it)
        }
    }

    private suspend fun updateUser(name: String, nickname: String) {
        val updateResult = runSuspendCatching {
            val result = updateUserAction(name, nickname)
            this._message.postValue("Name updated! Your new name is: '${result.name}'")
        }
        handleErrors(updateResult)
    }

    private suspend fun followUser(nickname: String, userToFollow: String) {
        val followResult = runSuspendCatching {
            val result = followUserAction(nickname, userToFollow)
            this._message.postValue("User '${result.userToFollow}' was followed")
        }
        handleErrors(followResult)
    }

    private suspend fun getFollowedUsers(nickname: String) {
        val getFollowedUsersResult = runSuspendCatching {
            val result = retrieveFollowedUsersAction(nickname)
            this._getMessages.postValue(result.followedUsers)
        }
        handleErrors(getFollowedUsersResult)
    }

    private suspend fun tweet(nickname: String, tweetMessage: String) {
        val tweetResult = runSuspendCatching {
            val result = userTweetAction(nickname, tweetMessage)
            this._message.postValue("Tweeted: '${result.tweet}'")
        }
        handleErrors(tweetResult)
    }

    private suspend fun getUserTweets(nickname: String) {
        val getUserTweetsResult = runSuspendCatching {
            val result = retrieveUserTweetsAction(nickname)
            this._getMessages.postValue(result.userTweets)
        }
        handleErrors(getUserTweetsResult)
    }

    private fun handleErrors(result: Result<Unit>) {
        result.onFailure {
            showError(it)
        }
    }

    private fun showError(it: Throwable) {
        _error.postValue(it.message)
    }
}
