package com.example.twitterkatafinal.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.twitterkatafinal.core.providers.ActionsProvider

@Suppress("UNCHECKED_CAST")
internal class TwitterViewModelFactory :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return TwitterViewModel(
            registerUserAction = ActionsProvider().createRegisterUser(),
            updateUserAction = ActionsProvider().createUpdateUser(),
            followUserAction = ActionsProvider().createFollowUser(),
            retrieveFollowedUsersAction = ActionsProvider().createRetrieveFollowedUsers(),
            userTweetAction = ActionsProvider().createUserTweet(),
            retrieveUserTweetsAction = ActionsProvider().createRetrieveUserTweets(),
        ) as T
    }
}