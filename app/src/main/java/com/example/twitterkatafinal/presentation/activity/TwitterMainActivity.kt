package com.example.twitterkatafinal.presentation.activity

import android.R
import android.os.Bundle
import android.text.TextUtils
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.twitterkatafinal.androidextensions.onChange
import com.example.twitterkatafinal.databinding.TwitterActivityMainBinding
import com.example.twitterkatafinal.presentation.viewmodel.TwitterViewModel
import com.example.twitterkatafinal.presentation.viewmodel.TwitterViewModelFactory

class TwitterMainActivity : AppCompatActivity() {
    private val twitterViewModel by viewModels<TwitterViewModel> { TwitterViewModelFactory() }
    private lateinit var binding: TwitterActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = TwitterActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        bindButtons()
        observeEvents()
    }

    private fun bindButtons() {
        val registerButton = binding.registerButton
        val updateButton = binding.updateButton
        val followButton = binding.followButton
        val tweetButton = binding.tweetButton
        val getFollowersButton = binding.getFollowersButton
        val getTweetsButton = binding.getTweetsButton

        registerButton.setOnClickListener {
            val registerUsernameInput = binding.registerUsernameInput.text.toString()
            val registerNicknameInput = binding.registerNicknameInput.text.toString()

            registerOrShowError(registerUsernameInput, registerNicknameInput)
        }

        updateButton.setOnClickListener {
            val updateUsernameInput = binding.updateNameInput.text.toString()
            val updateNicknameInput = binding.updateNicknameInput.text.toString()

            updateOrShowError(updateUsernameInput, updateNicknameInput)
        }

        followButton.setOnClickListener {
            val followerUsernameInput = binding.followerInput.text.toString()
            val followeeNicknameInput = binding.followeeInput.text.toString()

            followOrShowError(followerUsernameInput, followeeNicknameInput)
        }

        tweetButton.setOnClickListener {
            val tweetUsernameInput = binding.tweetUsernameInput.text.toString()
            val tweetMessageInput = binding.tweetMessageInput.text.toString()

            tweetOrShowError(tweetUsernameInput, tweetMessageInput)
        }

        getFollowersButton.setOnClickListener {
            val getFollowersOrTweetsInput = binding.getFollowersOrTweetsInput.text.toString()

            getFollowedOrShowError(getFollowersOrTweetsInput)
        }

        getTweetsButton.setOnClickListener {
            val getFollowersOrTweetsInput = binding.getFollowersOrTweetsInput.text.toString()

            getTweetsOrShowError(getFollowersOrTweetsInput)
        }
    }

    private fun observeEvents() {
        onChange(twitterViewModel.message) { message ->
            Toast.makeText(
                applicationContext,
                message,
                Toast.LENGTH_LONG
            ).show()

            binding.responseView.adapter = null
        }

        onChange(twitterViewModel.getMessages) { userInput ->

            val responseAdapter = ArrayAdapter(
                applicationContext,
                R.layout.simple_list_item_1,
                userInput
            )

            binding.responseView.adapter = responseAdapter
        }

        onChange(twitterViewModel.error) { errorMessage ->
            Toast.makeText(
                applicationContext,
                errorMessage,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun registerOrShowError(
        registerUsernameInput: String,
        registerNicknameInput: String
    ) {
        if (TextUtils.isEmpty(registerUsernameInput) || TextUtils.isEmpty(registerNicknameInput)) {
            Toast.makeText(
                this,
                "Name and Nickname are required to register a new user",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            binding.registerUsernameInput.text.clear()
            binding.registerNicknameInput.text.clear()

            twitterViewModel.onRegisterButtonClick(registerUsernameInput, registerNicknameInput)
        }
    }

    private fun updateOrShowError(
        updateUsernameInput: String,
        updateNicknameInput: String
    ) {
        if (TextUtils.isEmpty(updateUsernameInput) || TextUtils.isEmpty(updateNicknameInput)) {
            Toast.makeText(this, "Name and Nickname are required to update", Toast.LENGTH_SHORT)
                .show()
        } else {
            binding.updateNameInput.text.clear()
            binding.updateNicknameInput.text.clear()

            twitterViewModel.onUpdateButtonClick(updateUsernameInput, updateNicknameInput)
        }
    }

    private fun followOrShowError(
        followerUsernameInput: String,
        followeeNicknameInput: String
    ) {
        if (TextUtils.isEmpty(followerUsernameInput) || TextUtils.isEmpty(followeeNicknameInput)) {
            Toast.makeText(
                this,
                "Follower and Followee nicknames are required to follow",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            binding.followeeInput.text.clear()

            twitterViewModel.onFollowButtonClick(followerUsernameInput, followeeNicknameInput)
        }
    }

    private fun tweetOrShowError(
        tweetUsernameInput: String,
        tweetMessageInput: String
    ) {
        if (TextUtils.isEmpty(tweetUsernameInput) || TextUtils.isEmpty(tweetMessageInput)) {
            Toast.makeText(
                this,
                "Nickname and message are required to tweet",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            binding.tweetMessageInput.text.clear()

            twitterViewModel.onTweetButtonClick(tweetUsernameInput, tweetMessageInput)
        }
    }

    private fun getFollowedOrShowError(getFollowersOrTwitterInput: String) {
        if (TextUtils.isEmpty(getFollowersOrTwitterInput)) {
            Toast.makeText(
                this,
                "Nickname is required to view the user followers",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            binding.getFollowersOrTweetsInput.text.clear()

            twitterViewModel.onGetFollowedUsersButtonClick(getFollowersOrTwitterInput)
        }
    }

    private fun getTweetsOrShowError(getFollowersOrTwitterInput: String) {
        if (TextUtils.isEmpty(getFollowersOrTwitterInput)) {
            Toast.makeText(
                this,
                "Nickname is required to view the user tweets",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            binding.getFollowersOrTweetsInput.text.clear()

            twitterViewModel.onGetUserTweetsButtonClick(getFollowersOrTwitterInput)
        }
    }
}