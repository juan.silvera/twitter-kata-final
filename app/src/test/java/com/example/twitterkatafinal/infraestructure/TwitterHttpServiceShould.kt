package com.example.twitterkatafinal.infraestructure

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.twitterkatafinal.core.domain.FollowUserRequest
import com.example.twitterkatafinal.core.domain.RegisterRequest
import com.example.twitterkatafinal.core.domain.UpdateUserRequest
import com.example.twitterkatafinal.core.domain.UserTweetRequest
import com.example.twitterkatafinal.infraestructure.client.TwitterAPIClient
import com.example.twitterkatafinal.infraestructure.models.FollowUserResponse
import com.example.twitterkatafinal.infraestructure.models.RegisterUserResponse
import com.example.twitterkatafinal.infraestructure.models.RetrieveFollowedUsersResponse
import com.example.twitterkatafinal.infraestructure.models.UpdateUserResponse
import com.example.twitterkatafinal.infraestructure.service.TwitterHttpService
import com.example.twitterkatafinal.rules.CoroutineScopeRule
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Response

class TwitterHttpServiceShould {
    private lateinit var twitterHttpService: TwitterHttpService
    private lateinit var twitterAPIClient: TwitterAPIClient
    private var foundRetrieveFollowedUsersResponse: RetrieveFollowedUsersResponse? = null
    private var registerUserResult: RegisterUserResponse? = null
    private var updateUserResult: UpdateUserResponse? = null
    private var followUserResult: FollowUserResponse? = null

    @Rule
    @JvmField
    val instantTaskExecutor = InstantTaskExecutorRule()

    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    @Before
    fun setup() {
        twitterAPIClient = mock()
    }

    @Test
    fun `register a user`() = runBlocking {
        givenARegisterRequest(REGISTER_REQUEST)
        givenAnTwitterHttpService()

        whenRegisters(REGISTER_REQUEST)

        thenRegistersUser()
    }

    @Test
    fun `update user`() = runBlocking {
        givenAUserUpdateRequest(UPDATE_REQUEST)
        givenAnTwitterHttpService()

        whenUpdates(UPDATE_REQUEST)

        thenUpdatesUser()
    }

    @Test
    fun `follow user`() = runBlocking {
        givenAFollowRequest(FOLLOW_USER_REQUEST)
        givenAnTwitterHttpService()

        whenFollows(FOLLOW_USER_REQUEST)

        thenFollowsUser()
    }

    @Test
    fun `user tweet`() = runBlocking {
        givenAUserTweetRequest(USER_TWEET_REQUEST)
        givenAnTwitterHttpService()

        whenTweets(USER_TWEET_REQUEST)

        thenUserTweets()
    }

    @Test
    fun `get followed users`() = runBlocking {
        givenAFollowedRequest(NICKNAME)
        givenAnTwitterHttpService()

        whenGetsFollowedUsers(NICKNAME)

        assertEquals(FOLLOWED_USER_RESPONSE, foundRetrieveFollowedUsersResponse)
    }

    @Test
    fun `get tweets`() = runBlocking {
        givenAFollowedRequest(NICKNAME)
        givenAnTwitterHttpService()

        whenGetsFollowedUsers(NICKNAME)

        assertEquals(FOLLOWED_USER_RESPONSE, foundRetrieveFollowedUsersResponse)
    }

    private suspend fun givenARegisterRequest(request: RegisterRequest) {
        whenever(twitterAPIClient.register(request)).thenReturn(
            Response.success(
                200,
                """{"name":"pepito","nickname":"@pepito"}"""
            )
        )
    }

    private suspend fun givenAUserUpdateRequest(user: UpdateUserRequest) {
        whenever(twitterAPIClient.update(user)).thenReturn(
            Response.success(
                200,
                """{"name":"coquito","nickname":"@pepito"}"""
            )
        )
    }

    private suspend fun givenAFollowRequest(user: FollowUserRequest) {
        whenever(twitterAPIClient.follow(user)).thenReturn(
            Response.success(
                200,
                """{"nickname":"@pepito","userToFollow":"@hackerman"}"""
            )
        )
    }

    private suspend fun givenAUserTweetRequest(user: UserTweetRequest) {
        whenever(twitterAPIClient.tweet(user)).thenReturn(
            Response.success("""{"nickname":"@pepito","userTweet":"Tweeted: 'Hello world!'"}""")
        )
    }

    private suspend fun givenAFollowedRequest(nickname: String) {
        whenever(twitterAPIClient.followed(nickname)).thenReturn(
            Response.success("""{"followedUsers":["@pepito"]}""")
        )
    }

    private fun givenAnTwitterHttpService() {
        twitterHttpService = TwitterHttpService(twitterAPIClient)
    }

    private suspend fun whenRegisters(request: RegisterRequest) {
        registerUserResult = twitterHttpService.register(request)
    }

    private suspend fun whenUpdates(user: UpdateUserRequest) {
        updateUserResult = twitterHttpService.update(user)
    }

    private suspend fun whenFollows(user: FollowUserRequest) {
        followUserResult = twitterHttpService.follow(user)
    }

    private suspend fun whenTweets(user: UserTweetRequest) {
        twitterHttpService.tweet(user)
    }

    private suspend fun whenGetsFollowedUsers(nickname: String) {
        foundRetrieveFollowedUsersResponse = twitterHttpService.retrieveFollowedUsers(nickname)
    }

    private fun thenRegistersUser() {
        assertEquals(REGISTER_RESPONSE, registerUserResult)
    }

    private fun thenUpdatesUser() {
        assertEquals(UPDATE_RESPONSE, updateUserResult)
    }

    private suspend fun thenFollowsUser() {
        verify(twitterAPIClient, times(1)).follow(any())
    }

    private suspend fun thenUserTweets() {
        verify(twitterAPIClient, times(1)).tweet(any())
    }

    companion object {
        private const val NICKNAME = "@pepejr"
        private val REGISTER_REQUEST = RegisterRequest("pepito", "@pepito")
        private val REGISTER_RESPONSE = RegisterUserResponse("pepito", "@pepito")
        private val UPDATE_REQUEST = UpdateUserRequest("coquito", "@pepito")
        private val UPDATE_RESPONSE = UpdateUserResponse("coquito", "@pepito")
        private val FOLLOW_USER_REQUEST = FollowUserRequest("@pepito", "@guido")
        private val USER_TWEET_REQUEST = UserTweetRequest("@pepito", "Tweet!")
        private val FOLLOWED_USER_RESPONSE = RetrieveFollowedUsersResponse(mutableListOf("@pepito"))
    }
}