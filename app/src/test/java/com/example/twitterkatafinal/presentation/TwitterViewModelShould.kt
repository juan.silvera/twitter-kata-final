package com.example.twitterkatafinal.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.twitterkatafinal.core.actions.*
import com.example.twitterkatafinal.core.domain.*
import com.example.twitterkatafinal.core.service.UserService
import com.example.twitterkatafinal.infraestructure.models.*
import com.example.twitterkatafinal.presentation.viewmodel.TwitterViewModel
import com.example.twitterkatafinal.rules.CoroutineScopeRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

internal class TwitterViewModelShould {

    @Rule
    @JvmField
    val instantTaskExecutor = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    private lateinit var twitterViewModel: TwitterViewModel
    private lateinit var userService: UserService
    private lateinit var registerUser: RegisterUser
    private lateinit var updateUser: UpdateUser
    private lateinit var followUser: FollowUser
    private lateinit var retrieveFollowedUsers: RetrieveFollowedUsers
    private lateinit var userTweet: UserTweet
    private lateinit var retrieveUserTweets: RetrieveUserTweets

    @Before
    fun setup() {
        userService = mock()
        registerUser = RegisterUser(userService)
        updateUser = UpdateUser(userService)
        followUser = FollowUser(userService)
        retrieveFollowedUsers = RetrieveFollowedUsers(
            userService
        )
        userTweet = UserTweet(userService)
        retrieveUserTweets = RetrieveUserTweets(userService)
    }

    @Test
    fun `call register user action on button press`() = runBlocking {
        givenASuccessfulRegisterUser(NAME, NICKNAME)
        givenATwitterViewModel()

        whenClicksRegisterButton()

        thenShowsFeedbackMessage(EXPECTED_REGISTER_MESSAGE)
    }

    @Test
    fun `post error when user exists while registering`() = runBlocking {
        givenAFailingRegisterUser(NAME, NICKNAME)
        givenATwitterViewModel()

        whenClicksRegisterButton()

        thenRegisterShowsError()
    }

    @Test
    fun `call update user action on button press`() = runBlocking {
        givenASuccessfulUpdateUser(NAME, NICKNAME)
        givenATwitterViewModel()

        whenClicksUpdateButton()

        thenShowsFeedbackMessage(EXPECTED_UPDATE_MESSAGE)
    }

    @Test
    fun `post no user found error when updating`() = runBlocking {
        givenAFailingUpdateUser(NAME, NICKNAME)
        givenATwitterViewModel()

        whenUpdates(NAME, NICKNAME)

        thenShowsNoUserFoundError()
    }

    @Test
    fun `call follow user action on button press`() = runBlocking {
        givenASuccessfulFollowUser(NICKNAME, USER_TO_FOLLOW)
        givenATwitterViewModel()

        whenClicksFollowButton(NICKNAME, USER_TO_FOLLOW)

        thenShowsFeedbackMessage(EXPECTED_FOLLOW_MESSAGE)
    }

    @Test
    fun `post no user found error when following`() = runBlocking {
        givenAFailingFollowUser(NICKNAME, USER_TO_FOLLOW)
        givenATwitterViewModel()

        whenFollows(NICKNAME, USER_TO_FOLLOW)

        thenShowsNoUserFoundError()
    }

    @Test
    fun `call tweet user action on button press`() = runBlocking {
        givenASuccessfulUserTweet(NICKNAME, FIRST_TWEET)
        givenATwitterViewModel()

        whenClicksTweetButton(NICKNAME, FIRST_TWEET)

        thenShowsFeedbackMessage(EXPECTED_TWEET_MESSAGE)
    }

    @Test
    fun `post no user found error when tweeting`() = runBlocking {
        givenAFailingUserTweet(NICKNAME, FIRST_TWEET)
        givenATwitterViewModel()

        whenTweets(NICKNAME, FIRST_TWEET)

        thenShowsNoUserFoundError()
    }

    @Test
    fun `call get followed users action on button press`() = runBlocking {
        givenAStoredUserWithFollowers(NICKNAME)
        givenATwitterViewModel()

        whenGetsFollowedUsers(NICKNAME)

        thenGetsFollowedUsers(EXPECTED_FOLLOWED_USERS)
    }

    @Test
    fun `post no user found error when getting followed users`() = runBlocking {
        givenAFailingRetrieveFollowed(NICKNAME)
        givenATwitterViewModel()

        whenGetsFollowedUsers(NICKNAME)

        thenShowsNoUserFoundError()
    }

    @Test
    fun `call get tweets users action on button press`() = runBlocking {
        givenAStoredUserWithTweets(NICKNAME)
        givenATwitterViewModel()

        whenGetsUserTweets(NICKNAME)

        thenGetsUserTweets(EXPECTED_TWEETS)
    }

    @Test
    fun `post no user found error when getting user tweets`() = runBlocking {
        givenAFailingRetrieveTweets(NICKNAME)
        givenATwitterViewModel()

        whenGetsUserTweets(NICKNAME)

        thenShowsNoUserFoundError()
    }

    private fun givenATwitterViewModel() {
        twitterViewModel = TwitterViewModel(
            registerUser,
            updateUser,
            followUser,
            retrieveFollowedUsers,
            userTweet,
            retrieveUserTweets,
            Dispatchers.Main
        )
    }

    private suspend fun givenASuccessfulRegisterUser(name: String, nickname: String) {
        whenever(userService.register(RegisterRequest(name, nickname))).thenReturn(
            RegisterUserResponse(NAME, NICKNAME)
        )
    }

    private suspend fun givenAFailingRegisterUser(name: String, nickname: String) {
        whenever(userService.register(RegisterRequest(name, nickname))).thenReturn(
            null
        )
    }

    private suspend fun givenASuccessfulUpdateUser(name: String, nickname: String) {
        whenever(userService.update(UpdateUserRequest(name, nickname))).thenReturn(
            UpdateUserResponse(name, nickname)
        )
    }

    private suspend fun givenAFailingUpdateUser(name: String, nickname: String) {
        whenever(userService.update(UpdateUserRequest(name, nickname))).thenReturn(
            null
        )
    }

    private suspend fun givenASuccessfulFollowUser(nickname: String, userToFollow: String) {
        whenever(userService.follow(FollowUserRequest(nickname, userToFollow))).thenReturn(
            FollowUserResponse(nickname, userToFollow)
        )
    }

    private suspend fun givenAFailingFollowUser(nickname: String, userToFollow: String) {
        whenever(userService.follow(FollowUserRequest(nickname, userToFollow))).thenReturn(
            null
        )
    }

    private suspend fun givenASuccessfulUserTweet(nickname: String, userTweet: String) {
        whenever(userService.tweet(UserTweetRequest(nickname, userTweet))).thenReturn(
            UserTweetResponse(nickname, userTweet)
        )
    }

    private suspend fun givenAFailingUserTweet(nickname: String, userTweet: String) {
        whenever(userService.tweet(UserTweetRequest(nickname, userTweet))).thenReturn(
            null
        )
    }

    private suspend fun givenAStoredUserWithTweets(nickname: String) {
        whenever(userService.retrieveTweets(nickname)).thenReturn(USER_TWEET_RESPONSE)
    }

    private suspend fun givenAFailingRetrieveTweets(nickname: String) {
        whenever(userService.retrieveTweets(nickname)).thenReturn(
            null
        )
    }

    private suspend fun givenAStoredUserWithFollowers(nickname: String) {
        whenever(userService.retrieveFollowedUsers(nickname)).thenReturn(FOLLOWED_RESPONSE)
    }

    private suspend fun givenAFailingRetrieveFollowed(nickname: String) {
        whenever(userService.retrieveFollowedUsers(nickname)).thenReturn(
            null
        )
    }

    private fun whenClicksRegisterButton() {
        twitterViewModel.onRegisterButtonClick(NAME, NICKNAME)
    }

    private fun whenClicksUpdateButton() {
        twitterViewModel.onUpdateButtonClick(NAME, NICKNAME)
    }

    private fun whenClicksFollowButton(nickname: String, userToFollow: String) {
        twitterViewModel.onFollowButtonClick(nickname, userToFollow)
    }

    private fun whenClicksTweetButton(nickname: String, userTweet: String) {
        twitterViewModel.onTweetButtonClick(nickname, userTweet)
    }

    private fun whenUpdates(name: String, nickname: String) {
        twitterViewModel.onUpdateButtonClick(name, nickname)
    }

    private fun whenFollows(nickname: String, userToFollow: String) {
        twitterViewModel.onFollowButtonClick(nickname, userToFollow)
    }

    private fun whenTweets(nickname: String, tweet: String) {
        twitterViewModel.onTweetButtonClick(nickname, tweet)
    }

    private fun whenGetsFollowedUsers(nickname: String) {
        twitterViewModel.onGetFollowedUsersButtonClick(nickname)
    }

    private fun whenGetsUserTweets(nickname: String) {
        twitterViewModel.onGetUserTweetsButtonClick(nickname)
    }

    private fun thenRegisterShowsError() {
        assertEquals(EXPECTED_REGISTER_ERROR, twitterViewModel.error.value)
    }

    private fun thenShowsNoUserFoundError() {
        assertEquals(EXPECTED_NICKNAME_DOES_NOT_EXIST_ERROR, twitterViewModel.error.value)
    }

    private fun thenGetsFollowedUsers(expectedUserList: MutableList<String>) {
        assertEquals(expectedUserList, twitterViewModel.getMessages.value)
    }

    private fun thenGetsUserTweets(expectedTweets: MutableList<String>) {
        assertEquals(expectedTweets, twitterViewModel.getMessages.value)
    }

    private fun thenShowsFeedbackMessage(expectedMessage: String) {
        assertEquals(expectedMessage, twitterViewModel.message.value)
    }

    companion object {
        private const val NAME = "Pepito"
        private const val NICKNAME = "@pepito"
        private const val FIRST_TWEET = "A la maula!"
        private const val USER_TO_FOLLOW = "@CachitoCastaña"
        private const val EXPECTED_NICKNAME_DOES_NOT_EXIST_ERROR = "Your nickname does not exist"
        private const val EXPECTED_REGISTER_ERROR = "User already exists"
        private const val EXPECTED_REGISTER_MESSAGE = "Username '$NICKNAME' registered successfully"
        private const val EXPECTED_UPDATE_MESSAGE = "Name updated! Your new name is: '$NAME'"
        private const val EXPECTED_FOLLOW_MESSAGE = "User '$USER_TO_FOLLOW' was followed"
        private const val EXPECTED_TWEET_MESSAGE = "Tweeted: '$FIRST_TWEET'"

        val EXPECTED_FOLLOWED_USERS = mutableListOf("@hackerman", "@testeandin")
        val EXPECTED_TWEETS = mutableListOf("A la maula!")

        val USER_TWEET_RESPONSE = UserTweetsResponse(mutableListOf("A la maula!"))
        val FOLLOWED_RESPONSE =
            RetrieveFollowedUsersResponse(mutableListOf("@hackerman", "@testeandin"))
    }
}