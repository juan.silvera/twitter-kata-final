package com.example.twitterkatafinal.core.providers

import com.example.twitterkatafinal.core.actions.*
import com.example.twitterkatafinal.core.service.UserService
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class ActionsProviderShould {
    private lateinit var actionProvider: ActionsProvider
    private lateinit var userService: UserService
    private lateinit var registerUser: RegisterUser
    private lateinit var updateUser: UpdateUser
    private lateinit var followUser: FollowUser
    private lateinit var retrieveFollowedUsers: RetrieveFollowedUsers
    private lateinit var userTweet: UserTweet
    private lateinit var retrieveUserTweets: RetrieveUserTweets

    @Before
    fun setup() {
        userService = mock()
        actionProvider = ActionsProvider()
        registerUser = RegisterUser(userService)
        updateUser = UpdateUser(userService)
        followUser = FollowUser(userService)
        retrieveFollowedUsers = RetrieveFollowedUsers(userService)
        userTweet = UserTweet(userService)
        retrieveUserTweets = RetrieveUserTweets(userService)
    }

    @Test
    fun `create register action`() = runBlocking {
        registerUser = RegisterUser(userService)

        val createdAction = actionProvider.createRegisterUser()

        assertEquals(registerUser::class, createdAction::class)
    }

    @Test
    fun `create update user action`() = runBlocking {
        updateUser = UpdateUser(userService)

        val createdAction = actionProvider.createUpdateUser()

        assertEquals(updateUser::class, createdAction::class)
    }

    @Test
    fun `create follow user action`() = runBlocking {
        followUser = FollowUser(userService)

        val createdAction = actionProvider.createFollowUser()

        assertEquals(followUser::class, createdAction::class)
    }

    @Test
    fun `create retrieve followed users action`() = runBlocking {
        retrieveFollowedUsers = RetrieveFollowedUsers(userService)

        val createdAction = actionProvider.createRetrieveFollowedUsers()

        assertEquals(retrieveFollowedUsers::class, createdAction::class)
    }

    @Test
    fun `create user tweet action`() = runBlocking {
        userTweet = UserTweet(userService)

        val createdAction = actionProvider.createUserTweet()

        assertEquals(userTweet::class, createdAction::class)
    }

    @Test
    fun `create retrieve tweets action`() = runBlocking {
        retrieveUserTweets = RetrieveUserTweets(userService)

        val createdAction = actionProvider.createRetrieveUserTweets()

        assertEquals(retrieveUserTweets::class, createdAction::class)
    }
}

