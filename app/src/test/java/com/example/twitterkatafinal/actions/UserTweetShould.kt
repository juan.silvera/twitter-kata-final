package com.example.twitterkatafinal.actions

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.twitterkatafinal.core.actions.UserTweet
import com.example.twitterkatafinal.core.representation.UserTweetRepresentation
import com.example.twitterkatafinal.core.domain.UserTweetRequest
import com.example.twitterkatafinal.core.exceptions.TwitterKataException
import com.example.twitterkatafinal.core.service.UserService
import com.example.twitterkatafinal.infraestructure.models.UserTweetResponse
import com.example.twitterkatafinal.rules.CoroutineScopeRule
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class UserTweetShould {
    private lateinit var userService: UserService
    private lateinit var userTweet: UserTweet
    private lateinit var userTweetResult: UserTweetRepresentation

    @Rule
    @JvmField
    val instantTaskExecutor = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    @Before
    fun setup() {
        userService = mock()
    }

    @Test
    fun `save the user tweet`() = runBlocking {
        givenASuccessfulTweetResponse(USER_TWEET_RESPONSE)
        givenAUserTweet()

        whenUserTweets(NICKNAME, TWEET)

        thenTweets()
    }

    @Test(expected = TwitterKataException::class)
    fun `throw TwitterKataException with User does not exist error`() = runBlocking {
        givenAFailingTweetResponse()
        givenAUserTweet()

        whenUserTweets(NICKNAME, TWEET)
    }

    private fun givenAUserTweet() {
        userTweet = UserTweet(userService)
    }

    private suspend fun givenASuccessfulTweetResponse(userTweetResponse: UserTweetResponse) {
        whenever(userService.tweet(USER_TWEET_REQUEST)).thenReturn(
            userTweetResponse
        )
    }

    private suspend fun givenAFailingTweetResponse() {
        whenever(userService.tweet(USER_TWEET_REQUEST)).thenReturn(
            null
        )
    }

    private suspend fun whenUserTweets(nickname: String, userToFollowNickname: String) {
        userTweetResult = userTweet(nickname, userToFollowNickname)
    }

    private fun thenTweets() {
        assertEquals(TWEET, userTweetResult.tweet)
    }

    companion object {
        private const val NICKNAME = "@peTwitter"
        private const val TWEET = "Me gusta el arte!"
        private val USER_TWEET_REQUEST = UserTweetRequest(NICKNAME, TWEET)
        private val USER_TWEET_RESPONSE = UserTweetResponse(NICKNAME, TWEET)
    }
}