package com.example.twitterkatafinal.actions

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.twitterkatafinal.core.actions.RegisterUser
import com.example.twitterkatafinal.core.domain.RegisterRequest
import com.example.twitterkatafinal.core.representation.RegisterUserRepresentation
import com.example.twitterkatafinal.core.exceptions.TwitterKataException
import com.example.twitterkatafinal.core.service.UserService
import com.example.twitterkatafinal.infraestructure.models.RegisterUserResponse
import com.example.twitterkatafinal.rules.CoroutineScopeRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class RegisterUserShould {
    private lateinit var registerUser: RegisterUser
    private lateinit var userService: UserService
    private lateinit var registerUserResult: RegisterUserRepresentation

    @Rule
    @JvmField
    val instantTaskExecutor = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    @Before
    fun setup() {
        userService = mock()
    }

    @Test
    fun `save the user`() = runBlocking {
        givenASuccessfulRegisterResponse()
        givenARegisterUser()

        whenRegistersUser(NAME, NICKNAME)

        thenRegistersUser()
    }

    @Test(expected = TwitterKataException::class)
    fun `throw TwitterKataException when User already exists`() = runBlocking {
        givenAFailingRegisterResponse()
        givenARegisterUser()

        whenRegistersUser(NAME, NICKNAME)
    }

    private fun givenARegisterUser() {
        registerUser = RegisterUser(userService)
    }

    private suspend fun givenAFailingRegisterResponse() {
        whenever(userService.register(REGISTER_REQUEST)).thenReturn(
            null
        )
    }

    private suspend fun givenASuccessfulRegisterResponse() {
        whenever(userService.register(REGISTER_REQUEST)).thenReturn(REGISTER_RESPONSE)
    }

    private suspend fun whenRegistersUser(name: String, nickname: String) {
        registerUserResult = registerUser(name, nickname)
    }

    private fun thenRegistersUser() {
        assertEquals(EXPECTED_RESULT, registerUserResult)
    }

    companion object {
        private const val NAME = "Pepito Twitter"
        private const val NICKNAME = "@peTwitter"
        private val EXPECTED_RESULT = RegisterUserRepresentation(NICKNAME)
        private val REGISTER_REQUEST = RegisterRequest(NAME, NICKNAME)
        private val REGISTER_RESPONSE = RegisterUserResponse(NAME, NICKNAME)
    }
}