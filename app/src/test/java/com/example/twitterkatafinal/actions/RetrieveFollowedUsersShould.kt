package com.example.twitterkatafinal.actions

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.twitterkatafinal.core.actions.RetrieveFollowedUsers
import com.example.twitterkatafinal.core.exceptions.TwitterKataException
import com.example.twitterkatafinal.core.representation.RetrieveFollowedUsersRepresentation
import com.example.twitterkatafinal.core.service.UserService
import com.example.twitterkatafinal.infraestructure.models.RetrieveFollowedUsersResponse
import com.example.twitterkatafinal.rules.CoroutineScopeRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class RetrieveFollowedUsersShould {
    private lateinit var userService: UserService
    private lateinit var retrieveFollowedUsers: RetrieveFollowedUsers
    private lateinit var retrieveFollowedUsersResult: RetrieveFollowedUsersRepresentation

    @Rule
    @JvmField
    val instantTaskExecutor = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    @Before
    fun setup() {
        userService = mock()
    }

    @Test
    fun `retrieve followed users`() = runBlocking {
        givenASuccessfulFollowedUsersResponse(NICKNAME)
        givenAGetFollowers()

        whenGetsFollowedUsers(NICKNAME)

        thenGetsFollowedUsers()
    }

    @Test(expected = TwitterKataException::class)
    fun `throw TwitterKataException with User does not exist error`() = runBlocking {
        givenAFailingFollowedUsersResponse(NICKNAME)
        givenAGetFollowers()

        whenGetsFollowedUsers(NICKNAME)
    }

    private fun givenAGetFollowers() {
        retrieveFollowedUsers = RetrieveFollowedUsers(userService)
    }

    private suspend fun givenASuccessfulFollowedUsersResponse(nickname: String) {
        whenever(userService.retrieveFollowedUsers(nickname)).thenReturn(FOLLOWED_RESPONSE)
    }

    private suspend fun givenAFailingFollowedUsersResponse(nickname: String) {
        whenever(userService.retrieveFollowedUsers(nickname)).thenReturn(null)
    }

    private suspend fun whenGetsFollowedUsers(nickname: String) {
        retrieveFollowedUsersResult = retrieveFollowedUsers(nickname)
    }

    private fun thenGetsFollowedUsers() {
        assertEquals(EXPECTED_FOLLOWED_USERS, retrieveFollowedUsersResult.followedUsers)
    }

    companion object {
        private const val NICKNAME = "@peTwitter"
        private val FOLLOWED_RESPONSE =
            RetrieveFollowedUsersResponse(mutableListOf("@hackerman", "@thisisfine", "@lalala", "@elperri"))
        private val EXPECTED_FOLLOWED_USERS =
            mutableListOf("@hackerman", "@thisisfine", "@lalala", "@elperri")
    }
}