package com.example.twitterkatafinal.actions

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.twitterkatafinal.core.actions.UpdateUser
import com.example.twitterkatafinal.core.representation.UpdateUserRepresentation
import com.example.twitterkatafinal.core.domain.UpdateUserRequest
import com.example.twitterkatafinal.core.exceptions.TwitterKataException
import com.example.twitterkatafinal.core.service.UserService
import com.example.twitterkatafinal.infraestructure.models.UpdateUserResponse
import com.example.twitterkatafinal.rules.CoroutineScopeRule
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class UpdateUserShould {
    private lateinit var updateUser: UpdateUser
    private lateinit var userService: UserService
    private lateinit var updateUserResult: UpdateUserRepresentation

    @Rule
    @JvmField
    val instantTaskExecutor = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    @Before
    fun setup() {
        userService = mock()
    }

    @Test
    fun `update user name`() = runBlocking {
        givenAUpdateUser()
        givenASuccessfulUpdateResponse(UPDATED_USER_REQUEST)

        whenUpdatesUser(NEW_NAME, NICKNAME)

        thenUpdatesUserName()
    }

    @Test(expected = TwitterKataException::class)
    fun `throw TwitterKataException when User does not exist`() = runBlocking {
        givenAFailingUpdateResponse()
        givenAUpdateUser()

        whenUpdatesUser(NAME, NICKNAME)
    }

    private suspend fun givenAFailingUpdateResponse() {
        whenever(userService.update(UPDATED_USER_REQUEST)).thenReturn(
            null
        )
    }

    private suspend fun givenASuccessfulUpdateResponse(request: UpdateUserRequest) {
        whenever(userService.update(request)).thenReturn(UPDATE_USER_RESPONSE)
    }

    private fun givenAUpdateUser() {
        updateUser = UpdateUser(userService)
    }

    private suspend fun whenUpdatesUser(name: String, nickname: String) {
        updateUserResult = updateUser(name, nickname)
    }

    private fun thenUpdatesUserName() {
        assertEquals(NEW_NAME, updateUserResult.name)
    }

    companion object {
        private const val NAME = "Pepe Original"
        private const val NEW_NAME = "Pepito Updated"
        private const val NICKNAME = "@peTwitter"
        private val UPDATED_USER_REQUEST = UpdateUserRequest("Pepito Updated", NICKNAME)
        private val UPDATE_USER_RESPONSE = UpdateUserResponse(NAME, NICKNAME)
    }
}