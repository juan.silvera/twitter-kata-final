package com.example.twitterkatafinal.actions

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.twitterkatafinal.core.actions.RetrieveUserTweets
import com.example.twitterkatafinal.core.exceptions.TwitterKataException
import com.example.twitterkatafinal.core.representation.RetrieveUserTweetsRepresentation
import com.example.twitterkatafinal.core.service.UserService
import com.example.twitterkatafinal.infraestructure.models.UserTweetsResponse
import com.example.twitterkatafinal.rules.CoroutineScopeRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class RetrieveUserTweetsShould {
    private lateinit var userService: UserService
    private lateinit var retrieveUserTweets: RetrieveUserTweets
    private lateinit var retrieveUserTweetsResult: RetrieveUserTweetsRepresentation

    @Rule
    @JvmField
    val instantTaskExecutor = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    @Before
    fun setup() {
        userService = mock()
    }

    @Test
    fun `retrieve user tweets`() = runBlocking {
        givenASuccessfulUserTweetsResponse(NICKNAME)
        givenAGetUserTweets()

        whenGetsUserTweets(NICKNAME)

        thenGetsUserTweets()
    }

    @Test(expected = TwitterKataException::class)
    fun `throw TwitterKataException with User does not exist error`() = runBlocking {
        givenAFailingUserTweetsResponse(NICKNAME)
        givenAGetUserTweets()

        whenGetsUserTweets(NICKNAME)
    }

    private fun givenAGetUserTweets() {
        retrieveUserTweets = RetrieveUserTweets(userService)
    }

    private suspend fun givenASuccessfulUserTweetsResponse(nickname: String) {
        whenever(userService.retrieveTweets(nickname)).thenReturn(USER_TWEET_RESPONSE)
    }

    private suspend fun givenAFailingUserTweetsResponse(nickname: String) {
        whenever(userService.retrieveTweets(nickname)).thenReturn(null)
    }

    private suspend fun whenGetsUserTweets(nickname: String) {
        retrieveUserTweetsResult = retrieveUserTweets(nickname)
    }

    private fun thenGetsUserTweets() {
        assertEquals(USER_TWEETS, retrieveUserTweetsResult.userTweets)
    }

    companion object {
        private const val NICKNAME = "@peTwitter"
        private val USER_TWEETS = mutableListOf("Me gusta el arte!", "Que loco todo!")
        private val USER_TWEET_RESPONSE = UserTweetsResponse(USER_TWEETS)
    }
}