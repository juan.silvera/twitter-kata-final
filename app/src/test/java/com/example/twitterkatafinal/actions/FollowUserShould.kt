package com.example.twitterkatafinal.actions

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.twitterkatafinal.core.actions.FollowUser
import com.example.twitterkatafinal.core.representation.FollowUserRepresentation
import com.example.twitterkatafinal.core.domain.FollowUserRequest
import com.example.twitterkatafinal.core.exceptions.TwitterKataException
import com.example.twitterkatafinal.core.service.UserService
import com.example.twitterkatafinal.infraestructure.models.FollowUserResponse
import com.example.twitterkatafinal.rules.CoroutineScopeRule
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class FollowUserShould {
    private lateinit var userService: UserService
    private lateinit var followUser: FollowUser
    private lateinit var followUserResult: FollowUserRepresentation


    @Rule
    @JvmField
    val instantTaskExecutor = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineScope = CoroutineScopeRule()

    @Before
    fun setup() {
        userService = mock()
    }

    @Test
    fun `follow a user`() = runBlocking {
        givenASuccessfulFollowResponse(FOLLOW_USER_RESPONSE)
        givenAFollowUser()

        whenFollowsUser(NICKNAME, USER_TO_FOLLOW_NICKNAME)

        thenFollowsUser()
    }

    @Test(expected = TwitterKataException::class)
    fun `throw TwitterKataException with User does not exist error`() = runBlocking {
        givenAFailingFollowResponse()
        givenAFollowUser()

        whenFollowsUser(NICKNAME, USER_TO_FOLLOW_NICKNAME)
    }

    private fun givenAFollowUser() {
        followUser = FollowUser(userService)
    }

    private suspend fun givenASuccessfulFollowResponse(followUserResponse: FollowUserResponse) {
        whenever(userService.follow(FOLLOW_USER_REQUEST)).thenReturn(
            followUserResponse
        )
    }

    private suspend fun givenAFailingFollowResponse() {
        whenever(userService.follow(FOLLOW_USER_REQUEST)).thenReturn(
            null
        )
    }

    private suspend fun whenFollowsUser(nickname: String, userToFollowNickname: String) {
        followUserResult = followUser(nickname, userToFollowNickname)
    }

    private fun thenFollowsUser() {
        assertEquals(USER_TO_FOLLOW_NICKNAME, followUserResult.userToFollow)
    }

    companion object {
        private const val NICKNAME = "@peTwitter"
        private const val USER_TO_FOLLOW_NICKNAME = "@hackerman"
        private val FOLLOW_USER_REQUEST =
            FollowUserRequest(NICKNAME, USER_TO_FOLLOW_NICKNAME)
        private val FOLLOW_USER_RESPONSE = FollowUserResponse(
            NICKNAME, USER_TO_FOLLOW_NICKNAME
        )
    }
}